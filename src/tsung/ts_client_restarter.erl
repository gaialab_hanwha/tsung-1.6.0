%%%-------------------------------------------------------------------
%%% @author ktz
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 18. 4월 2016 오후 5:17
%%%-------------------------------------------------------------------
-module(ts_client_restarter).
-author("ktz").
-include("ts_config.hrl").
-include("ts_profile.hrl").
-behaviour(gen_server).

-define(RESTART_INTERVAL, 1000).
-define(RECONNECT_NUM, 20).
%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {reconnect_timer}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([]) ->
  ?DebugF("Init Client_Restarter~n", []),
  ets:new(ts_client, [set, named_table]),
%%  TRef = erlang:send_after(?RESTART_INTERVAL, self(), request_random_restart),
%%  {ok, #state{reconnect_timer = TRef}}.
  {ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call(_Request, _From, State) ->
  {reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast({request_add_me, {UserID, Pid}}, State) ->
  ets:insert(ts_client, {UserID, Pid}),
  {noreply, State};

handle_cast({request_delete_me, Id}, State) ->
  ets:delete(ts_client, Id),
  {noreply, State};

handle_cast({request_start, Client_State}, State) ->
  ?DebugF("[minchul] Start Again : ~p~n", [Client_State#state_rcv.id]),
  start_ts_client(Client_State),
  {noreply, State};

handle_cast(_Request, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(request_random_restart, State) ->
  ClientList = ets:tab2list(ts_client),
  if
    length(ClientList) < ?RECONNECT_NUM -> ok;
    true ->
      random:seed(now()),
      ReconnectList = getRadomClientList([], ?RECONNECT_NUM, length(ClientList)),
      ?DebugF("[minchul] request_random_restart : ~w~n", [ReconnectList]),
      lists:foreach(
        fun(ClientNum) ->
          {_, Pid} = lists:nth(ClientNum, ClientList),
          gen_fsm:send_all_state_event(Pid, reconnect)
        end, ReconnectList)

%%      {_, _, Micro} = random:seed(now()),
%%      {_Id, Pid} = lists:nth((Micro rem length(ClientList)) + 1, ClientList),
%%      ?DebugF("[minchul] request_random_restart : ~p~n", [_Id]),
%%      gen_fsm:send_all_state_event(Pid, reconnect)
  end,
  TRef = erlang:send_after(?RESTART_INTERVAL, self(), request_random_restart),
  {noreply, State#state{reconnect_timer = TRef}};

handle_info(_Info, State) ->
  {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================



start_ts_client(Client_State) ->
  ts_client_sup:start_child({#session{
    id = Client_State#state_rcv.session_id,
    persistent = Client_State#state_rcv.persistent,
    bidi = Client_State#state_rcv.bidi,
    hibernate = Client_State#state_rcv.hibernate,
    rate_limit = Client_State#state_rcv.rate_limit,
    proto_opts = Client_State#state_rcv.proto_opts,
    size = Client_State#state_rcv.maxcount,
    client_ip = {{0,0,0,0}, 0},
    userid = Client_State#state_rcv.id,
    dump = Client_State#state_rcv.dump,
    seed = now,
    server = #server{port = Client_State#state_rcv.port, host = Client_State#state_rcv.host, type = ts_tcp},
    type = Client_State#state_rcv.clienttype
  }, Client_State#state_rcv.session}).

getRadomClientList(ClientList, Num, ClientLen) ->
  if
    Num == 0 ->
      lists:usort(ClientList);
    true ->
      {_, _, Micro} = random:seed(now()),
      getRadomClientList([(Micro rem ClientLen) + 1 | ClientList], Num - 1, ClientLen)
  end.